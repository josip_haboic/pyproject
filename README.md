# pyproject


## Install

**Minimum Python version required is: >=3.6**

1.Download this package

2.Go to downloaded folder, next open command prompt and do this command:

```shell
pip install .
```

3.Place site-packages/pyproject to your PATH

4.Open command prompt:

```shell
python -m pyproject
```

---

input "help" and press enter

For now you can start new python projects
and initialize them in few ways:

## Examples

Create new project "example":

``` shell
project new example
```

Initialize project with git and python distribution files:

``` shell
project init
```

Initialize project with git only:

``` shell
project init nopydist
```

Initialize project with pip only:

``` shell
project init nogit
```

Initialize project without any extras:

``` shell
project init nodist
```
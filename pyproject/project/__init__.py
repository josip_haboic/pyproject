from os import mkdir, chdir
from os.path import exists
from sys import version
from json import load as json_load
from json import dump as json_dump
from .defaults import (
    DEFAULT_IGNORE_LIST, DEFAULT_SUBFOLDERS,
    SETUP_PY_TEMPLATE, DEV_REQUIREMENTS
)
from textwrap import dedent
from platform import system


PLATFORM_SYSTEM = system()
del system


def seed(project_name=None) -> bool:
    """
    If there is no pyproject.json in this folder,
    create one and write in defaults, then prompt user to edit this file.
    If there is one, return False to indicate the project is already seeded.
    """
    if exists("pyproject.json"):
        return False

    if not project_name:
        project_name = input("Project name:")

    project_name = project_name.replace("-", "_").replace(" ", "_").lower()
    project_description = input("Project short description:")
    project_url = input("Project url:")
    project_author = input("Project author:")
    project_email = input("Project authors email:")
    project_py_version = input("Project Py version:")
    project_licence = input("Project licence:")

    if not project_py_version:
        project_py_version = ''.join(['>=py'] + version[0:3].split('.'))
    if not project_licence:
        project_licence = "MIT"

    with open(
        "pyproject.json",
        mode="w",
        encoding="utf-8"
    ) as project_json:
        json_dump({
            'project': {
                'name': project_name if project_name else "unnamed_project",
                'description': project_description,
                'url': project_url,
                'author': project_author,
                'email': project_email,
                'py': project_py_version,
                'licence': project_licence
            }
        }, project_json, indent=2)
    return True


def new(project_name) -> bool:
    """ Create new folder/space for this project. """
    project_name = project_name.replace("-", "_").replace(" ", "_").lower()

    if not exists(project_name):
        mkdir(project_name)
        chdir(project_name)
        return True
    return False


def get_project_metadata():
    try:
        with open(
            "pyproject.json",
            mode="r",
            encoding="utf-8"
        ) as project_metadata:
            PROJECT_METADATA = json_load(project_metadata)["project"]
    except FileNotFoundError:
        return None

    if not PROJECT_METADATA:
        return False

    # dash/space is not allowed silently convert to underscore
    PROJECT_METADATA['name'] = \
        PROJECT_METADATA['name'].replace("-", "_").replace(" ", "_").lower()

    return PROJECT_METADATA


def init(**options) -> bool:
    """
    Create needed folders and files.
    These include among others:
        git files with defaults
        python packaging and requirement files
        README.md
    """
    PROJECT_METADATA = get_project_metadata()
    if PROJECT_METADATA is False:
        return False
    if PROJECT_METADATA is None:
        return None

    mkdir(PROJECT_METADATA['name'])

    with open(
        f"{PROJECT_METADATA['name']}/__init__.py",
        mode="w",
        encoding="utf-8"
    ) as project_init:
        project_init.close()

    with open(
        f"{PROJECT_METADATA['name']}/__version__.py",
        mode="w",
        encoding="utf-8"
    ) as project_init:
        project_init.write(dedent("""
            MAJOR = 0
            MINOR = 0
            RELEASE = 1
            VERSION = f"{MAJOR}.{MINOR}.{RELEASE}"
        """))

    for folder in DEFAULT_SUBFOLDERS:
        mkdir(folder)

    if options.get("git", True):
        with open(".gitignore", "w") as gitignore:
            for ignored in DEFAULT_IGNORE_LIST:
                gitignore.write(ignored)
                gitignore.write("\n")
            gitignore.close()
        with open(".gitattributes", "w") as gitattributes:
            gitattributes.close()

    if options.get("pydist", True):
        ext = "bat" if PLATFORM_SYSTEM == "Windows" else "sh"
        with open("requirements.txt", "w") as project_requirements:
            project_requirements.close()
        with open(".developer/requirements.txt", "w") as dev_requirements:
            dev_requirements.write(DEV_REQUIREMENTS)
            dev_requirements.close()
        with open("setup.cfg", "w") as setup_cfg:
            setup_cfg.close()
        with open("setup.py", "w") as setup_py:
            setup_py.write(SETUP_PY_TEMPLATE.format(**PROJECT_METADATA))
            setup_py.close()
        with open(f"start_pytest.{ext}", "w") as start_pytest:
            start_pytest.write("pytest -v > .developer/pytest.txt")
            start_pytest.close()
        with open(f"start_coverage.{ext}", "w") as start_coverage:
            start_coverage.write(
                "coverage report -m > .developer/coverage.txt"
            )
            start_coverage.close()

    with open("README.md", "w") as readme:
        readme.close()

    return True

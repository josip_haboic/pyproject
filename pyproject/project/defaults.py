# default project folders
DEFAULT_SUBFOLDERS = [
    ".developer",
    "tests",
]

# default ignore list
DEFAULT_IGNORE_LIST = [
    ".ignore",
    ".vscode",
    "**.log",
    "**.pyc",
    "__pycache__/",
    "**.egg-info/",
    "dist",
    "venv",
    "build"
]

# template for setup.py
SETUP_PY_TEMPLATE = """\
from setuptools import setup, find_packages
from {name}.__version__ import VERSION

NAME = "{name}"
DESCRIPTION = "{description}"
URL = "{url}"
EMAIL = "{email}"
AUTHOR = "{author}"
REQUIRES_PYTHON = "{py}"
REQUIREMENTS = None
with open("./requirements.txt", mode="r", encoding="utf-8") as requirements:
    REQUIREMENTS = [r for r in requirements.readlines()]
README = None
with open("./README.md", mode="r", encoding="utf-8") as readme:
    README = readme.read()
PACKAGE_DATA = dict({name}=[])
EXCLUDE_PACKAGES = []

setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    long_description=README,
    author=AUTHOR,
    author_email=EMAIL,
    url=URL,
    package_data=PACKAGE_DATA,
    packages=find_packages(exclude=EXCLUDE_PACKAGES),
    install_requires=REQUIREMENTS,
    license="{licence}"
)
"""

DEV_REQUIREMENTS = """\
pytest
coverage
"""

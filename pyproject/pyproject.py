from . import project


PROJECT_NAME = None


HELP = """
+--------------------------------------------------
| pyproject cli help
+ -------------------------------------------------
| Supported commands:
| project new {name}    - new project in folder "{name}"
| project               - treat current folder as new project space
| project init          - initialize project
| project init nogit    - initialize project without git
| project init nopydist - initialize project without setup.py and friends
| project init nodist   - initialize project without distribution components
+--------------------------------------------------
"""


def input_cli(message=""):
    return input("| pyproject: {}".format(message))


def print_cli(message):
    print("| pyproject: {}".format(message))


def cli_project_init(**options):
    init = project.init(**options)
    if init is True:
        print_cli("OK:")
        print_cli("PROJECT INITIALIZED")
    elif init is None:
        print_cli("WARNING:")
        print_cli("PROJECT NEEDS THIS FILE: project.yaml")
        if input_cli(
            "SEED PROJECT? (yes|no): "
        ) == "yes":
            project.seed()
            print_cli("PLEASE EDIT project.yaml TO YOURS NEEDS")
            print_cli("AFTER YOU FINISH DO: project init")
    else:
        print_cli("ERROR:")
        print_cli("PROJECT SEEMS TO BE ALREADY INITIALIZED")
        print_cli("PLEASE EDIT project.yaml")


def project_cli(cmd):
    CMD_LEN = len(cmd)

    if cmd[0] == "project":

        if CMD_LEN == 1:
            if project.seed("default-project-name"):
                print_cli("OK:")
                print_cli("SEEDED NEW PROJECT")
            else:
                print_cli("ERROR:")
                print_cli("CANNOT SEED EXISTING PROJECT")
                print_cli("FILE project.yaml ALREADY EXISTS")
                print_cli("TO INITIALIZE DO: project init")

        if CMD_LEN == 2:
            if cmd[1] == "init":
                cli_project_init()

        if CMD_LEN == 3:
            if cmd[1] == "new":
                project.new(cmd[2])
                project.seed(cmd[2])
                print_cli("OK:")
                print_cli("NEW PROJECT CREATED")
                print_cli("NEW PROJECT SEEDED")
            elif cmd[1] == "init":
                if cmd[2] == "nogit":
                    cli_project_init(git=False)
                elif cmd[2] == "nopydist":
                    cli_project_init(pydist=False)
                elif cmd[2] == "nodist":
                    cli_project_init(pydist=False, git=False)


def andormeda_cli():
    WELCOME_MESSAGE = "| === Welcome to the pyproject CLI === |"
    print("|" + ("=" * (len(WELCOME_MESSAGE) - 2)) + "|")
    print(WELCOME_MESSAGE)
    print("|" + ("=" * (len(WELCOME_MESSAGE) - 2)) + "|")
    print("|")

    cmd = None

    while True:
        try:
            user_input = input_cli()

            cmd = user_input.lower().split(" ")

            if len(cmd) == 0:
                continue

            selected = cmd[0]

            if selected == "project":
                project_cli(cmd)
                continue
            elif selected == "help":
                print_cli(HELP)
            elif selected == "exit":
                raise SystemExit
            else:
                print_cli("UNKNOWN COMMAND: {}".format(selected))

            print("| " + (32 * "-"))

        except KeyboardInterrupt:
            print_cli("...bye...bye...")
            raise SystemExit

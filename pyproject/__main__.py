# python3
# -*- coding:utf-8 -*-
#
# File: __main__.py
# Project: pyproject
# Created Date: Friday 30-03-2018 21:41:13
# Author: Josip Haboic
# E-mail: josiphaboic@gmail.com
# Last Modified: Friday 30-03-2018 21:47:46
# Modified By: Josip Haboic
#
from .pyproject import andormeda_cli


if __name__ == "__main__":
    andormeda_cli()
# -*- encoding: utf-8 -*-
from setuptools import setup, find_packages
from pyproject.__version__ import VERSION


NAME = "pyproject"

DESCRIPTION = "Python project CLI"

URL = "https://bitbucket.org/josip_haboic/pyproject"

EMAIL = "josiphaboic@gmail.com"

AUTHOR = "Josip Haboić"

REQUIRES_PYTHON = ">=3.6.0"

REQUIREMENTS = None
with open("./project-requirements.txt", "r") as requirements:
    REQUIREMENTS = [r for r in requirements.readlines()]

README = None
with open("./README.md", "r") as readme:
    README = readme.read()

PACKAGE_DATA = {}


setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    long_description=README,
    author=AUTHOR,
    author_email=EMAIL,
    url=URL,
    package_data=PACKAGE_DATA,
    packages=find_packages(),
    install_requires=REQUIREMENTS,
    license="GNU General Public License"
)
